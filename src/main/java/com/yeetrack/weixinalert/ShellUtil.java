package com.yeetrack.weixinalert;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Xuemeng Wang on 15/4/2.
 */
public class ShellUtil {
    DefaultExecutor defaultExecutor;
    ByteArrayOutputStream outputStream;
    ByteArrayOutputStream errorStream;
    PumpStreamHandler pumpStreamHandler;

    public ShellUtil()
    {
        defaultExecutor = new DefaultExecutor();
        outputStream = new ByteArrayOutputStream();
        errorStream = new ByteArrayOutputStream();
        pumpStreamHandler = new PumpStreamHandler(outputStream, errorStream);
        defaultExecutor.setStreamHandler(pumpStreamHandler);
    }

    public void execute(CommandLine commandLine)
    {
        try {
            defaultExecutor.execute(commandLine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getOutAsString()
    {
        return outputStream.toString();
    }
    public String getErrorAsString()
    {
        return errorStream.toString();
    }
}
