package com.yeetrack.weixinalert;

import org.apache.commons.exec.CommandLine;

import java.io.*;

/**
 * Created by Xuemeng Wang on 15/3/27.
 */
public class Alert {
    /**
     * 微信心跳，最好设置一分钟一次，防止微信服务器，踢掉此会话
     */
    public void syncWeixinHeart()
    {
        //还是直接调用shell吧，省事
        String shellPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        String shellDir = shellPath.substring(0, shellPath.lastIndexOf(File.separator)+1);
        String shpath= shellDir+"weixin-heart.sh";
        System.out.println("heart shell --->"+shpath);
        try {
            ShellUtil shellUtil = new ShellUtil();
            CommandLine commandLine = new CommandLine("sh");
            commandLine.addArgument(shpath);
            shellUtil.execute(commandLine);
            String result = shellUtil.getOutAsString();

            //Logger.debug(sb.toString().trim());
            if(result.contains("{\n" +
                    "\"BaseResponse\": {\n" +
                    "\"Ret\": 0,\n" +
                    "\"ErrMsg\": \"\"\n" +
                    "}"))
                System.out.println("微信心跳成功");
            else
                System.out.println("微信心跳失败");

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    /**
     * 发送微信消息
     * @return
     */
    public void sendWeixinMsg(String content)
    {
        //Logger.debug(content);
        if(null==content || "".equals(content))
            return;
        content = content.replace("\\", "\\\\").replace("\"", "\\\"");

        //修改shell，将content写进去
        String shellPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        String shellDir = shellPath.substring(0, shellPath.lastIndexOf(File.separator)+1);
        String line = null;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream inputStream = new FileInputStream(new File(shellDir + "weixin-send.sh"));
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line).append("\n");
            }
            reader.close();
            inputStreamReader.close();
            inputStream.close();
            int contentStartPos = stringBuffer.indexOf("\"Content\":\"");
            int contentEndPos = stringBuffer.indexOf("\",\"", contentStartPos + "\"Content\":\"".length()+1);
            stringBuffer.replace(contentStartPos+"\"Content\":\"".length(), contentEndPos, content);

            BufferedWriter out = new BufferedWriter(new FileWriter(shellDir + "weixin-send.sh", false));
            System.out.println(out.toString());
            out.write(stringBuffer.toString());
            //Logger.debug(stringBuffer.toString());
            out.close();

        } catch (Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        String shpath= shellDir+"weixin-send.sh";
        try {
            ShellUtil shellUtil = new ShellUtil();
            CommandLine commandLine = new CommandLine("sh");
            commandLine.addArgument(shpath);
            shellUtil.execute(commandLine);
            String result = shellUtil.getOutAsString();
            if(result.contains("{\n" +
                    "\"BaseResponse\": {\n" +
                    "\"Ret\": 0,\n" +
                    "\"ErrMsg\": \"\"\n" +
                    "}"))
                System.out.println("微信发送成功");
            else
                System.out.println("微信发送失败");

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("传递参数有误，只需要一个参数:content");
            return;
        }
        Alert alert = new Alert();
        alert.syncWeixinHeart();
        alert.sendWeixinMsg(args[0]);
    }
}
